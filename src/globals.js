export const PlayMode = Object.freeze({
  Typing: 0,
  Random: 1
});

export function getRandomFromList(list) {
  return list[Math.floor(Math.random() * list.length)]
}
