# Tube Station Game

## A React.js and Leaflet map based game to find tube stations

Public URL: www.findthetube.com
Dev URL: https://jamesfowkes.gitlab.io/tube-station-game

## Help / About

[Visit the game](www.findthetube.com) and click the "?" logo in the map.

## Attributions

 - Station data adapted from [https://www.doogal.co.uk/london_stations](https://www.doogal.co.uk/london_stations) by Chris Bell
 - Lines data adapted from data at [https://github.com/oobrien/vis/tree/master/tubecreature](https://github.com/oobrien/vis/tree/master/tubecreature) by Oliver O'Brien
 - TFL roundel from [https://en.wikipedia.org/wiki/File:TfL_roundel_(no_text).svg](https://en.wikipedia.org/wiki/File:TfL_roundel_(no_text).svg) by Dream out loud
 - Icons adapted from [https://glyphs.fyi/](https://glyphs.fyi/) by Goran
 - "Railway sans" font by [Greg Fleming](https://github.com/davelab6/Railway-Sans)
 